#!/bin/bash

# Остановить контейнер my-nginx, если он существует
if [ "$(docker ps -q -f name=my-nginx)" ]; then
    docker stop my-nginx
fi

# Загрузить новый образ
docker pull registry.gitlab.com/george.sulim/jenkins_task/my-nginx:latest

# Запустить контейнер на основе нового образа
docker run -p 0.0.0.0:80:80 -d --rm --name my-nginx registry.gitlab.com/george.sulim/jenkins_task/my-nginx:latest
